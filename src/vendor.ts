import * as turf from '@turf/turf'
import { Feature as OLFeature } from 'ol'
import Collection from 'ol/Collection'
import OLPolygon from 'ol/geom/Polygon'
import SimpleGeometry from 'ol/geom/SimpleGeometry'

import { castles, perimeterLine, POIs } from './data'
import { cut } from './utils'


const getCircle = (myFeature: OLFeature, radius = 0.2) => {
    const point1 = turf.point((myFeature.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates());
    return turf.circle(point1, radius, {steps: 24, units: 'kilometers', properties: {foo: 'bar'}})
        .geometry
        .coordinates;
}

const getSquare = (myFeature: OLFeature, radius = 0.2) => {
    const point1 = turf.point((myFeature.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates());
    const topRight = turf.transformTranslate(point1, radius, 45, {units: 'kilometers'})
    const bottomRight = turf.transformTranslate(point1, radius, 135, {units: 'kilometers'})
    const bottomLeft = turf.transformTranslate(point1, radius, -135, {units: 'kilometers'})
    const topLeft = turf.transformTranslate(point1, radius, -45, {units: 'kilometers'})

    const result = turf.lineString([
        topRight   .geometry.coordinates,
        bottomRight.geometry.coordinates,
        bottomLeft .geometry.coordinates,
        topLeft    .geometry.coordinates,
        topRight   .geometry.coordinates,
    ]).geometry.coordinates;
    return result
}

/**
 * Calculate the zone from a center point of a zone.
 *
 * On paper you would draw perpendicular lines between a center and neighbor's centers, then connect those lines.
 * In a function it's not so easy to understand who's neighbor, so just draw lines every perpendicular but idea stays.
 */
const getZone = (myFeature: OLFeature, POIs: Collection<OLFeature>) => {
    const myPoint = turf.point((myFeature.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates());
    const polygonPerimeter = turf.polygon((perimeterLine.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getCoordinates());

    const myCircle = turf.polygon([myFeature.getProperties().circle.clone().transform('EPSG:3857', 'EPSG:4326').getCoordinates()[1]])
    const myCircleCropped = turf.intersect(myCircle, polygonPerimeter) || myCircle

    const result = POIs.getArray()
        .map((poi) => {
            const toPoint = turf.point((poi.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates());
            const distance = turf.distance(myPoint, toPoint)
            if(distance > 0.3) return null

            const midpoint = turf.midpoint(myPoint, toPoint);
            const line = turf.lineString([myPoint.geometry.coordinates,toPoint.geometry.coordinates]);
            const rotatedPoly = turf.transformRotate(line, 90, {pivot:midpoint});
            const scaledPoly = turf.transformScale(rotatedPoly, 5, {origin:midpoint});
            return scaledPoly
        })
        .filter(Boolean)
        .reduce(
            (remainder, line) => cut(remainder, line, myPoint),
            myCircleCropped
        )
        .geometry
        .coordinates;

    return result
}

export const updateCircle = (poi: OLFeature) => {
    poi.setProperties({
        bubbleCollect: new OLFeature(new OLPolygon([getCircle(poi, 0.02)[0]]).transform('EPSG:4326', 'EPSG:3857')).getGeometry(),
        bubbleLoot: new OLFeature(new OLPolygon([getSquare(poi, 0.02)]).transform('EPSG:4326', 'EPSG:3857')).getGeometry(),
        circle: new OLFeature(new OLPolygon([getCircle(poi, 0.1)[0], getCircle(poi, 0.2)[0]]).transform('EPSG:4326', 'EPSG:3857')).getGeometry(),
    })
}

export const updateZone = (poi: OLFeature) => {
    poi.setProperties({
        zone: new OLFeature(new OLPolygon(getZone(poi, POIs)).transform('EPSG:4326', 'EPSG:3857')).getGeometry(),
    })
}

export const updateFill = (feature: OLFeature) => {
    if(feature.getProperties().type.includes('castle')){
        feature.setProperties({
            colors: [{color: feature.getProperties().color, distance: 1, weight: 0}],
        })
        return
    }

    const point1 = turf.point((feature.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates());
    const colorsRaw = castles.getArray().map((castle) => {
        const point2 = turf.point((castle.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates());
        return {
            color: castle.getProperties().color as string,
            distance: turf.distance(point1, point2),
        }
    })
    const colorsMid = colorsRaw.map(({color, distance}) => ({
        color,
        distance,
        weightRaw: 1 / Math.pow(distance, 2),
    }))
    const weightRawTotal = colorsMid.reduce((sum, {weightRaw}) => sum + weightRaw, 0)
    const colors = colorsMid.map(({color, distance, weightRaw}) => ({
        color,
        distance,
        weight: weightRaw / weightRawTotal,
    }))
    feature.setProperties({
        colors,
    })
}

export const updateProximity = (poi: OLFeature) => {
    const properties = poi.getProperties()
    if(!properties.type.includes('castle')) return []

    const proximity = POIs.getArray()
        .filter((poi) => poi.getProperties().type !== 'leather')  // Ignore Leather
        .reduce((sum, poi) => {
            return sum + Math.pow((poi.getProperties().colors.find(({color})=> color === properties.color) || {weight: 0}).weight, 2)
        }, 0)

    poi.setProperties({
        proximity,
    })
}
