import SimpleGeometry from 'ol/geom/SimpleGeometry'
import { perimeterLine, POIs } from './data'

const exportPOIs = () => {
    const json = POIs.getArray().map((poi) => {
        const id = poi.getId()
        const properties = poi.getProperties()

        return {
            id,
            type: properties.type,
            epsg4326: (poi.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getFlatCoordinates().map((coord) => Number(coord.toFixed(6))),
            color: properties.color,
        }
    })
    console.info(json)
}
window['exportPOIs'] = exportPOIs

const exportPerimeter = () => {
    const jsonRaw = (perimeterLine.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326') as SimpleGeometry).getCoordinates()[0]
    const jsonRounded = jsonRaw.map((coords) => coords.map((coord) => Number(coord.toFixed(6))))
    console.info(jsonRounded)
}
window['exportPerimeter'] = exportPerimeter
