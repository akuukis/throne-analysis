import * as turf from '@turf/turf'
import { Feature, LineString, Polygon } from '@turf/turf'
import polygonSplitter from 'polygon-splitter'

export const cut = (polygon: Feature<Polygon>, line: Feature<LineString>, center = turf.center(polygon)) => {
    const outcome = polygonSplitter(polygon.geometry, line.geometry)

    if(outcome.type === 'Polygon') return turf.feature(outcome)

    const mainPolygon = outcome.geometry.coordinates
        .map((positions) => turf.polygon(positions))
        .find((polygon) => turf.booleanPointInPolygon(center, polygon))

    return mainPolygon
}
